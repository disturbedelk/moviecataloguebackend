﻿using ASPNetProj.DAL;
using ASPNetProj.DAL.DTO;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace ASPNetProj.Controllers
{
    [Route("api/account/profile")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly DatabaseContext Context;
        private readonly IConfiguration Configuration;

        public UserController(DatabaseContext context, IConfiguration configuration)
        {
            Context = context;
            Configuration = configuration;
        }

        [HttpGet]
        [Authorize]
        public async Task<IActionResult> GetUserProfile()
        {
            if (User.Identity == null || User.Identity.Name == null)
            {
                return BadRequest(new { message = Configuration["Localization:Response:InvalidAuth"] });
            }
            try
            {
                var user = await Context.Users.FirstOrDefaultAsync(x => x.Id == Guid.Parse(User.Identity.Name));

                if (user == null)
                {
                    return NotFound(new { message = Configuration["Localization:Response:NoSuchProfile"] });
                }
                var response = new ProfileModel()
                {
                    Id = user.Id,
                    NickName = user.Username,
                    Email = user.Email,
                    AvatarLink = user.AvatarLink,
                    Name = user.Name,
                    BirthDate = user.BirthDate,
                    Gender = user.Gender
                };
                return new JsonResult(response);
            }
            catch (Exception ex)
            {
                return BadRequest();
            }
        }

        [HttpPut]
        [Authorize]
        public async Task<IActionResult> UpdateUserProfile([FromBody] ProfileModel model)
        {
            if (User.Identity == null || User.Identity.Name == null)
            {
                return BadRequest(new { message = Configuration["Localization:Response:InvalidAuth"] });
            }
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                var user = await Context.Users.FirstOrDefaultAsync(x => x.Id == Guid.Parse(User.Identity.Name));


                if (user == null)
                {
                    return NotFound(new { message = Configuration["Localization:Response:NoSuchProfile"] });
                }
                if (user.Id != model.Id)
                {
                    return Unauthorized(new { message = Configuration["Localization:Response:BadProfileId"] });
                }
                //user.Id = model.Id;
                user.AvatarLink = model.AvatarLink;
                user.Name = model.Name;
                user.BirthDate = model.BirthDate;
                user.Gender = model.Gender;
                user.Email = model.Email;

                await Context.SaveChangesAsync();
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest();
            }
        }
    }
}
