﻿using ASPNetProj.DAL;
using ASPNetProj.DAL.DTO;
using ASPNetProj.DAL.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics.Metrics;
using System.Diagnostics;
using System.IO;
using System.Xml.Linq;
using ASPNetProj.Services.Interfaces;

namespace ASPNetProj.Controllers
{
    [Route("api/movies")]
    [ApiController]
    public class MovieController : ControllerBase
    {
        private readonly DatabaseContext Context;
        private readonly IPageManager PageManager;
        private readonly IMovieModelHelper MovieHelper;
        public MovieController(DatabaseContext context, IPageManager pageManager, IMovieModelHelper movieHelper)
        {
            Context = context;
            PageManager = pageManager;
            MovieHelper = movieHelper;
        }
        [HttpGet("details/{id}")]
        public async Task<IActionResult> GetMovieDetails([FromRoute] Guid id)
        {
            //check for authorization
            if (User.Identity != null && User.Identity.Name != null)
            {
                var user = await Context.Users.FirstOrDefaultAsync(user => user.Id == Guid.Parse(User.Identity.Name));
            }

            var userId = (User.Identity != null && User.Identity.Name != null) ?
                Guid.Parse(User.Identity.Name)
                : Guid.Empty;

            var response = await GetMovieModel(id, userId);
            if (response == null)
            {
                return NotFound();
            }

            return Ok(response);
        }

        [HttpGet("{page}")]
        public async Task<IActionResult> GetMoviePage([FromRoute] int page)
        {
            try
            {
                var response = await PageManager.RequestMoviePage(page);
                return Ok(response);
            }
            catch (Exception ex)
            {
                return NotFound();
            }
        }

        [HttpPut]
        [Authorize(Roles = "admin")]
        public async Task<IActionResult> AddMovie(MovieDetailsModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            try
            {
                var exists = false;
                var movie = await Context.Movies.Include(x => x.Genres).FirstOrDefaultAsync(x => x.Id == model.Id);
                if (movie == null)
                {
                    movie = new Movie();
                }
                else exists = true;

                await MovieHelper.FillMovieFromModel(movie, model);

                if (!exists)
                {
                    Context.Movies.Add(movie);
                }
                await Context.SaveChangesAsync();
                PageManager.SetDirty();
                return Ok(movie.Id);
            }
            catch (Exception ex)
            {
                return BadRequest();
            }
        }

        private async Task<MovieDetailsModel?> GetMovieModel(Guid id, Guid? requestingUser = null)
        {
            var movie = await Context.Movies.Include(x => x.Genres).Include(x => x.Reviews).FirstOrDefaultAsync(x => x.Id == id);
            if (movie == null)
            {
                return null;
            }
            var result = new MovieDetailsModel();
            await MovieHelper.FillModelFromMovie(movie, result, requestingUser);
            return result;
        }
    }
}
