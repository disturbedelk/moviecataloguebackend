﻿using ASPNetProj.DAL;
using ASPNetProj.DAL.DTO;
using ASPNetProj.DAL.Models;
using ASPNetProj.Services.Interfaces;
using Microsoft.AspNet.Identity;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.JsonWebTokens;
using Microsoft.IdentityModel.Tokens;
using System.ComponentModel.DataAnnotations;
using System.IdentityModel.Tokens.Jwt;
using System.Linq.Expressions;
using System.Security.Claims;
using System.Security.Principal;
using System.Text;

namespace ASPNetProj.Controllers
{
    [Route("api/account/")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private readonly DatabaseContext Context;
        private readonly IConfiguration Configuration;
        private readonly ITokenBlocklistManager Blocklist;
        private readonly ITokenService TokenService;
        private readonly IPasswordHasher PasswordHasher;
        public AuthController(DatabaseContext context,
                              IConfiguration configuration,
                              ITokenBlocklistManager blocklistManager,
                              ITokenService tokenService,
                              IPasswordHasher passwordHasher)
        {
            Context = context;
            Configuration = configuration;
            Blocklist = blocklistManager;
            TokenService = tokenService;
            PasswordHasher = passwordHasher;
            Blocklist.EnableCleanup();
        }
        [HttpPost("register")]
        public async Task<IActionResult> Register([FromBody] UserRegisterModel model)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }
                if(await Context.Users.FirstOrDefaultAsync(x => x.Username == model.Username) != null)
                {
                    return BadRequest(new { message = Configuration["Localization:Response:UserExists"] });
                }
                Context.Users.Add(new User
                {
                    Id = new Guid(),
                    Name = model.Name,
                    Email = model.Email,
                    Password = PasswordHasher.HashPassword(model.Password),
                    BirthDate = model.BirthDate,
                    Username = model.Username,
                    Gender = model.Gender,
                    IsAdmin = false,
                    AvatarLink = Configuration["Defaults:AvatarURL"]

                });
                await Context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception occured during registration " + ex.Message);
                return NoContent();
            }

            var response = new LoginCredentialsModel { Username = model.Username, Password = model.Password };
            return Login(response);
        }

        [HttpPost("login")]
        public IActionResult Login([FromBody] LoginCredentialsModel model)
        {
            var user = Context.Users.FirstOrDefault(x => (x.Username == model.Username));
            var responseFail = BadRequest(new { message = Configuration["Localization:Response:LoginFail"] });

            try
            {
                if (user == null ||
                    PasswordHasher.VerifyHashedPassword(user.Password, model.Password) == PasswordVerificationResult.Failed)
                {
                    return responseFail;
                }
            }
            catch (FormatException ex)
            {
                return responseFail;
            }

            try
            {
                var jwtEncoded = TokenService.BuildToken(TokenService.GetIdentity(user));
                var response = new
                {
                    token = jwtEncoded
                };

                return new JsonResult(response);
            }
            catch (ArgumentException ex)
            {
                //return BadRequest(new { message = Configuration["Localization:Response:LoginFail"], details = ex.Message });
                return responseFail;
            }
        }

        [HttpPost("logout")]
        [Authorize]
        public async Task<IActionResult> Logout()
        {
            try
            {
                var token = Request.Headers.Authorization.FirstOrDefault()?.Split(" ").Last() ?? "";
                Context.Blocklist.Add(new Token()
                {
                    Id = Guid.NewGuid(),
                    Value = token
                });
                await Context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception occured during logout: " + ex.Message);
            }
            var response = new { token = "", message = Configuration["Localization:Response:Logout"] };
            return new JsonResult(response);
        }

        #region BLOCKLIST
        [Authorize(Roles = "admin")]
        [HttpPost("blocklist/cleanup/disable")]
        public IActionResult DisableCleanup()
        {
            Blocklist.DisableCleanup();
            return Ok();
        }

        [Authorize(Roles = "admin")]
        [HttpPost("blocklist/cleanup/enable")]
        public IActionResult EnableCleanup()
        {
            Blocklist.EnableCleanup();
            return Ok();
        }

        [Authorize(Roles = "admin")]
        [HttpPost("blocklist/list")]
        public async Task<IActionResult> ListTokens()
        {
            var tokens = await Context.Blocklist.ToListAsync();
            return new JsonResult(tokens);
        }

        [Authorize(Roles = "admin")]
        [HttpPost("blocklist/testing/addExpiredToken/{quantity}")]
        public async Task<IActionResult> AddExpiredToken([FromRoute][Range(1, 10)] int quantity)
        {
            await AddEmptyTokensWithLifetime(quantity, TimeSpan.FromSeconds(1));
            return Ok();
        }

        [Authorize(Roles = "admin")]
        [HttpPost("blocklist/testing/addUsualToken/{quantity}")]
        public async Task<IActionResult> AddNormalToken([FromRoute][Range(1, 10)] int quantity)
        {
            await AddEmptyTokensWithLifetime(quantity, new TimeSpan(0, 1, 30));
            return Ok();
        }

        private async Task AddEmptyTokensWithLifetime(int quantity, TimeSpan lifetime)
        {
            var tokens = new List<Token>();
            for (int count = 0; count < quantity; ++count)
            {
                tokens.Add(new Token()
                {
                    Id = Guid.NewGuid(),
                    Value = GetEmptyToken(lifetime),
                });
            }
            Context.Blocklist.AddRange(tokens);
            await Context.SaveChangesAsync();
        }

        private string GetEmptyToken(TimeSpan lifetime)
        {
            var now = DateTime.UtcNow;
            var jwt = new JwtSecurityToken(
               issuer: "blank",
               audience: "blank",
            notBefore: now,
               expires: now.Add(lifetime),
               signingCredentials: new SigningCredentials(
                    new SymmetricSecurityKey(Encoding.ASCII.GetBytes("very much a valid key 000000000000")),
                    SecurityAlgorithms.HmacSha256));
            var encodedJwt = new JwtSecurityTokenHandler().WriteToken(jwt);
            return encodedJwt;
        }
        #endregion

    }
}
