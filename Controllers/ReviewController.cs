﻿using ASPNetProj.DAL;
using ASPNetProj.DAL.DTO;
using ASPNetProj.DAL.Models;
using ASPNetProj.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Diagnostics;

namespace ASPNetProj.Controllers
{
    [Route("api/movie/{movieId}/review")]
    [ApiController]
    public class ReviewController : ControllerBase
    {
        private readonly DatabaseContext Context;
        private readonly IPageManager PageManager;
        public ReviewController(DatabaseContext context, IPageManager pageManager)
        {
            Context = context;
            PageManager = pageManager;
        }

        [HttpPost("add")]
        [Authorize]
        public async Task<IActionResult> AddReview([FromRoute] Guid movieId, [FromBody] ReviewModifyModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (User.Identity == null || User.Identity.Name == null)
            {
                return Unauthorized();
            }
            try
            {
                var user = await Context.Users.FirstOrDefaultAsync(user => user.Id == Guid.Parse(User.Identity.Name));
                if (user == null)
                {
                    return Unauthorized();
                }

                var movie = await Context.Movies.FirstOrDefaultAsync(movie => movie.Id == movieId);
                if (movie == null)
                {
                    return NotFound(new { message = $"Movie ID {movieId} was not found" });
                }

                if (await Context.Reviews.FirstOrDefaultAsync(review =>
                    review.MovieId == movie.Id
                    && review.UserId == user.Id) != null)
                {
                    return BadRequest(new { message = "You have already left a review for this movie" });
                }

                var review = new Review()
                {
                    Id = Guid.NewGuid(),
                    UserId = user.Id,
                    MovieId = movie.Id,
                    CreateDateTime = DateTime.Now,
                    IsAnonymous = model.IsAnonymous,
                    ReviewText = model.ReviewText,
                    Rating = model.Rating
                };
                await Context.Reviews.AddAsync(review);
                await Context.SaveChangesAsync();
                PageManager.SetDirty();
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest();
            }
        }

        [HttpPut("{id}/edit")]
        [Authorize]
        public async Task<IActionResult> EditReview(
            [FromRoute] Guid movieId,
            [FromRoute] Guid id,
            [FromBody] ReviewModifyModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (User.Identity == null || User.Identity.Name == null)
            {
                return Unauthorized();
            }
            try
            {
                var user = await Context.Users.FirstOrDefaultAsync(user => user.Id == Guid.Parse(User.Identity.Name));
                if (user == null)
                {
                    return Unauthorized();
                }

                var movie = await Context.Movies.FirstOrDefaultAsync(movie => movie.Id == movieId);
                if (movie == null)
                {
                    return NotFound(new { message = $"Movie ID {movieId} was not found" });
                }

                var review = await Context.Reviews.FirstOrDefaultAsync(review =>
                    review.MovieId == movie.Id
                    && review.Id == id);

                if (review == null)
                {
                    return NotFound(new { message = $"Review id {id} was not found" });
                }

                if (review.UserId != user.Id && !User.IsInRole("admin"))
                {
                    return StatusCode(StatusCodes.Status405MethodNotAllowed);
                }

                review.ReviewText = model.ReviewText;
                review.Rating = model.Rating;
                review.IsAnonymous = model.IsAnonymous;

                await Context.SaveChangesAsync();
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest();
            }
        }

        [HttpDelete("{id}/delete")]
        [Authorize]
        public async Task<IActionResult> DeleteReview(
            [FromRoute] Guid movieId,
            [FromRoute] Guid id)
        {
            if (User.Identity == null || User.Identity.Name == null)
            {
                return Unauthorized();
            }
            try
            {
                var user = await Context.Users.FirstOrDefaultAsync(user => user.Id == Guid.Parse(User.Identity.Name));
                if (user == null)
                {
                    return Unauthorized();
                }

                var movie = await Context.Movies.FirstOrDefaultAsync(movie => movie.Id == movieId);
                if (movie == null)
                {
                    return NotFound(new { message = $"Movie ID {movieId} was not found" });
                }

                var review = await Context.Reviews.FirstOrDefaultAsync(review =>
                    review.MovieId == movie.Id
                    && review.Id == id);

                if (review == null)
                {
                    return NotFound(new { message = $"Review id {id} was not found" });
                }

                if (review.UserId != user.Id && !User.IsInRole("admin"))
                {
                    return StatusCode(StatusCodes.Status405MethodNotAllowed);
                }

                Context.Reviews.Remove(review);
                await Context.SaveChangesAsync();
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest();
            }
        }

    }
}
