﻿using ASPNetProj.DAL;
using ASPNetProj.DAL.DTO;
using ASPNetProj.DAL.Models;
using ASPNetProj.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace ASPNetProj.Controllers
{
    [Route("api/favorites")]
    [ApiController]
    public class FavoriteMoviesController : ControllerBase
    {
        private readonly DatabaseContext Context;
        private readonly IMovieModelHelper MovieHelper;

        public FavoriteMoviesController(DatabaseContext context, IMovieModelHelper modelHelper)
        {
            Context = context;
            MovieHelper = modelHelper;
        }

        [HttpGet]
        [Authorize]
        public async Task<IActionResult> GetFavorites()
        {
            if (User.Identity == null || User.Identity.Name == null)
            {
                return Unauthorized();
            }

            var favorites = await GetUserFavorites(Guid.Parse(User.Identity.Name));

            if(favorites == null)
            {
                return BadRequest();
            }

            return Ok(favorites);
        }

        [HttpGet("{profileId}")]
        [Authorize]
        public async Task<IActionResult> GetFavorites([FromRoute] Guid profileId)
        {
            var favorites = await GetUserFavorites(profileId);

            if (favorites == null)
            {
                return NotFound(new { message = $"User {profileId} was not found" });
            }

            return Ok(favorites);
        }

        [HttpPost("{id}/add")]
        [Authorize]
        public async Task<IActionResult> AddFavorite([FromRoute] Guid id)
        {
            if (User.Identity == null || User.Identity.Name == null)
            {
                return Unauthorized();
            }
            try
            {
                var user = await Context.Users.Include(x => x.Favorites)
                    .FirstOrDefaultAsync(user => user.Id == Guid.Parse(User.Identity.Name));
                if (user == null)
                {
                    return Unauthorized();
                }

                var movie = await Context.Movies.FirstOrDefaultAsync(movie => movie.Id == id);
                if (movie == null)
                {
                    return NotFound(new { message = $"Movie id {id} was not found" });
                }

                var userFavorites = user.Favorites as List<Movie>;
                if (userFavorites == null)
                {
                    userFavorites = new List<Movie>();
                }
                if (userFavorites.Find(x => x.Id == movie.Id) != null)
                {
                    return BadRequest(new { message = "Movie already added to favorites" });
                }
                user.Favorites.Add(movie);
                await Context.SaveChangesAsync();
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest();
            }
        }

        [HttpDelete("{id}/delete")]
        [Authorize]
        public async Task<IActionResult> DeleteFavorite([FromRoute] Guid id)
        {
            if (User.Identity == null || User.Identity.Name == null)
            {
                return Unauthorized();
            }
            try
            {
                var user = await Context.Users.Include(x => x.Favorites)
                    .FirstOrDefaultAsync(user => user.Id == Guid.Parse(User.Identity.Name));

                if (user == null)
                {
                    return Unauthorized();
                }

                var favorites = user.Favorites as List<Movie> ?? new List<Movie>();
                var element = favorites.Find(x => x.Id == id);
                if (element == null)
                {
                    return BadRequest(new { message = "Not-existing user favorite movie" });
                }

                favorites.Remove(element);
                await Context.SaveChangesAsync();
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest();
            }
        }

        private async Task<MoviesListModel?> GetUserFavorites(Guid userId)
        {
            try
            {
                var user = await Context.Users.Include(x => x.Favorites)
                    .FirstOrDefaultAsync(user =>
                    user.Id == userId);

                if (user == null)
                {
                    return null;
                }

                var movieIds = new List<Guid>(user.Favorites.Count);

                foreach (var movie in user.Favorites)
                {
                    movieIds.Add(movie.Id);
                }

                var favorites = new MoviesListModel();

                foreach (var movie in await Context.Movies
                    .Include(x => x.Genres)
                    .Include(x => x.Reviews)
                    .Where(x => movieIds.Contains(x.Id))
                    .ToListAsync())
                {
                    var model = new MovieElementModel();
                    MovieHelper.FillModelFromMovie(movie, model);
                    favorites.Movies.Add(model);
                }
                return favorites;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception occured when retrieving user favorites " + ex.Message);
                return null;
            }
        }
    }
}
