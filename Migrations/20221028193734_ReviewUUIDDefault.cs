﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace ASPNetProj.Migrations
{
    public partial class ReviewUUIDDefault : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<Guid>(
                name: "Id",
                table: "Reviews",
                type: "uniqueidentifier",
                nullable: false,
                oldDefaultValue: new Guid("00000000-0000-0000-0000-000000000000"),
                defaultValue: null);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
