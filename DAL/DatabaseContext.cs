﻿using ASPNetProj.DAL.Models;
using Microsoft.EntityFrameworkCore;
namespace ASPNetProj.DAL
{
    public class DatabaseContext : DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<Movie> Movies { get; set; }
        public DbSet<Review> Reviews { get; set; }
        public DbSet<Genre> Genres { get; set; }
        public DbSet<Token> Blocklist { get; set; }
        public DatabaseContext(DbContextOptions<DatabaseContext> options) : base(options)
        {
            Database.EnsureCreated();
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>()
                .HasIndex(u => u.Username)
                .IsUnique();
            modelBuilder.Entity<Review>()//alt: unique
                .HasKey(obj => new { obj.UserId, obj.MovieId });

            modelBuilder.Entity<Movie>()
                .HasMany(movie => movie.Genres)
                .WithMany(genre => genre.Movies);
            modelBuilder.Entity<Movie>()
                .HasMany(movie => movie.FavoriteOf)
                .WithMany(user => user.Favorites);
            modelBuilder.Entity<Genre>()
                .HasMany(genre => genre.Movies)
                .WithMany(movie => movie.Genres);
        }
    }
}
