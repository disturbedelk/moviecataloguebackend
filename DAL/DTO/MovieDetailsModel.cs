﻿using ASPNetProj.DAL.DTO;
using System.ComponentModel.DataAnnotations;
using System.Security.Permissions;

namespace ASPNetProj.DAL.Models
{
    public class MovieDetailsModel
    {
        public Guid Id { get; set; }
        [Required]
        public string Name { get; set; }
        [Url]
        public string? Poster { get; set; }
        public int Year { get; set; }
        public string? Country { get; set; }
        public ICollection<GenreModel> ?Genres { get; set; } = new List<GenreModel>();
        public ICollection<ReviewModel>? Reviews { get; set; } = new List<ReviewModel>();
        public int Time { get; set; }
        public string? Tagline { get; set; }
        public string? Description { get; set; }
        public string? Director { get; set; }
        public int? Budget { get; set; }
        public int? Fees { get; set; }
        public int AgeLimit { get; set; }
    }
}
