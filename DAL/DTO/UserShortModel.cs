﻿using System.ComponentModel.DataAnnotations;

namespace ASPNetProj.DAL.DTO
{
    public class UserShortModel
    {
        [Required]
        public Guid UserId{ get; set; }
        public string? NickName { get; set; }
        [Url]
        public string? Avatar { get;set; }
    }
}
