﻿using ASPNetProj.DAL.Models;

namespace ASPNetProj.DAL.DTO
{
    public class MoviesPagedListModel
    {
        public PageInfoModel PageInfo { get; set; }
        public ICollection<MovieElementModel> Movies { get; set; } = new List<MovieElementModel>();
    }
}
