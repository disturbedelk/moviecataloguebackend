﻿using System.ComponentModel.DataAnnotations;

namespace ASPNetProj.DAL.DTO
{
    public class ReviewModel
    {
        public Guid Id { get; set; }
        public int Rating { get; set; }
        public string? ReviewText { get; set; }
        public bool IsAnonymous { get; set; }
        public DateTime CreateDateTime { get; set; }
        [Required]
        public UserShortModel Author { get; set; }
    }
}
