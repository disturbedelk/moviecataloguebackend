﻿using System.ComponentModel.DataAnnotations;

namespace ASPNetProj.DAL.DTO
{
    public class GenreModel
    {
        public Guid Id { get; set; }
        [Required]
        public string Name { get; set; } = String.Empty;
    }
}
