﻿using ASPNetProj.DAL.Models;

namespace ASPNetProj.DAL.DTO
{
    public class MoviesListModel
    {
        public List<MovieElementModel> Movies { get; set; } = new List<MovieElementModel>();
    }
}
