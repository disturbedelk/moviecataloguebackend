﻿namespace ASPNetProj.DAL.DTO
{
    public class ReviewShortModel
    {
        public Guid Id { get; set; }
        public int Rating { get; set; } 
    }
}
