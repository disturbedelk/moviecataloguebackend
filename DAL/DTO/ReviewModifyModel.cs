﻿using System.ComponentModel.DataAnnotations;

namespace ASPNetProj.DAL.DTO
{
    public class ReviewModifyModel
    {
        [Required]
        public string ReviewText { get; set; } = String.Empty;
        [Range(0, 10)]
        public int Rating { get; set; } = 0;
        public bool IsAnonymous { get; set; } = true;
    }
}
