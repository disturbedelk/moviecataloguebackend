﻿using System.ComponentModel.DataAnnotations;
using ASPNetProj.DAL.Models;

namespace ASPNetProj.DAL.DTO
{
    public class ProfileModel
    {
        public Guid Id { get; set; }
        public string NickName{ get; set; }
        [Required]
        [EmailAddress]
        public string Email { get; set; }
        [Url]     
        public string AvatarLink { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public DateTime BirthDate { get; set; }
        public Gender Gender { get; set; }
    }
}
