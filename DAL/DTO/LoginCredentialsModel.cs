﻿namespace ASPNetProj.DAL.DTO
{
    public class LoginCredentialsModel
    {
        public string Username { get; set; }
        public string Password { get; set; }
    }
}
