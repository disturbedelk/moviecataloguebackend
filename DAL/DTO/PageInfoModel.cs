﻿using System.ComponentModel.DataAnnotations;

namespace ASPNetProj.DAL.DTO
{
    public class PageInfoModel
    {
        public int PageSize { get; set; }
        public int PageCount { get; set; }
        public int CurrentPage { get; set; }
    }
}
