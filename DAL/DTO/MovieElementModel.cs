﻿using ASPNetProj.DAL.DTO;
using System.ComponentModel.DataAnnotations;
using System.Security.Permissions;

namespace ASPNetProj.DAL.Models
{
    public class MovieElementModel
    {
        public Guid Id { get; set; }
        [Required]
        public string Name { get; set; }
        [Url]
        public string? Poster { get; set; }
        public int Year { get; set; }
        public string? Country { get; set; }
        public ICollection<GenreModel> Genres { get; set; } = new List<GenreModel>();
        public ICollection<ReviewShortModel> Reviews { get; set; } = new List<ReviewShortModel>();
    }
}
