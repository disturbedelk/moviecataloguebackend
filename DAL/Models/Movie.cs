﻿using System.ComponentModel.DataAnnotations;
using System.Security.Policy;

namespace ASPNetProj.DAL.Models
{
    public class Movie
    {
        [Key]
        public Guid Id { get; set; } = new Guid();
        [Required]
        [StringLength(150)]
        public string Name { get; set; } = "Unnamed Movie";
        public string? Poster { get; set; }
        public string? Description { get; set; }
        public int Year { get; set; }
        public string? Country { get; set; }
        public int Time { get; set; }
        public string? Tagline { get; set; }
        public string? Director { get; set; }
        public int? Budget { get; set; }
        public int? Fees { get; set; }
        public int AgeLimit { get; set; }
        public ICollection<Genre> Genres { get; set; } = new List<Genre>();
        public ICollection<Review> Reviews { get; set; } = new List<Review>();
        public ICollection<User> FavoriteOf { get; set; } = new List<User>();
    }
}
