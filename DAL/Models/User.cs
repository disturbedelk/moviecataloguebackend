﻿using System.ComponentModel.DataAnnotations;
using System.Runtime.InteropServices;

namespace ASPNetProj.DAL.Models
{
    public class User
    {
        [Key]
        public Guid Id { get; set; }
        [StringLength(50)]
        public string Name { get; set; } = "Unnamed user";
        public DateTime BirthDate { get; set; }
        [Required]
        [StringLength(100)]
        public string Email { get; set; }
        [Required]
        [StringLength(50)]
        public string Username { get; set; }
        [Required]
        public string Password { get; set; }
        public string AvatarLink { get; set; }
        public Gender Gender { get; set; }
        [Required]
        public bool IsAdmin { get; set; }
        public ICollection<Review> Reviews { get; set; } = new List<Review>();
        public ICollection<Movie> Favorites { get; set; } = new List<Movie>();
    }
}
