﻿using System.ComponentModel.DataAnnotations;

namespace ASPNetProj.DAL.Models
{
    public class Genre
    {
        [Key]
        public Guid Id { get; set; }
        [Required]
        [StringLength(50)]
        public string Name { get; set; } = "";
        public ICollection<Movie> Movies { get; set; } = new List<Movie>();
    }
}
