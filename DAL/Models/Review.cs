﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ASPNetProj.DAL.Models
{
    public class Review
    {
        [Key]
        public Guid Id { get; set; }
        [Required]
        [ForeignKey("User")]
        public Guid UserId { get; set; }
        [Required]
        [ForeignKey("Movie")]
        public Guid MovieId{ get; set; }
        public string? ReviewText { get; set; }
        public int Rating { get; set; }
        public bool IsAnonymous{ get; set; }
        public DateTime CreateDateTime{ get; set; }
    }
}
