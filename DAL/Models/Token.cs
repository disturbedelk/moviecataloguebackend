﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ASPNetProj.DAL.Models
{
    public class Token
    {
        [Key]
        public Guid Id { get; set; } = new Guid();
        public string Value { get; set; } = String.Empty;
    }
}
