﻿using ASPNetProj.DAL;
using ASPNetProj.DAL.Models;
using ASPNetProj.Services.Interfaces;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Security.Principal;
using System.Text;

namespace ASPNetProj.Services
{
    public class UserTokenService : ITokenService
    {
        public readonly string Issuer;
        public readonly string Audience;
        public readonly int Lifetime;
        public SymmetricSecurityKey SymmetricSecurityKey => new SymmetricSecurityKey(Encoding.ASCII.GetBytes(Key));
        private readonly string Key;
        public UserTokenService(IConfiguration configuration)
        {
            Issuer = configuration["Jwt:Issuer"];
            Audience = configuration["Jwt:Audience"];
            Key = configuration["Jwt:Key"];
            Lifetime = Convert.ToInt32(configuration["Jwt:Lifetime"]);
        }

        public string BuildToken(ClaimsIdentity identity)
        {
            var now = DateTime.UtcNow;

            var jwt = new JwtSecurityToken(
                issuer: Issuer,
                audience: Audience,
                notBefore: now,
                claims: identity.Claims,
                expires: now.Add(new TimeSpan(Lifetime)),
                signingCredentials: new SigningCredentials(SymmetricSecurityKey,
                                                           SecurityAlgorithms.HmacSha256));
            var encodedJwt = new JwtSecurityTokenHandler().WriteToken(jwt);
            return encodedJwt;
        }
        public ClaimsIdentity GetIdentity<T>(T identityProvider)
        {
            if(identityProvider is not User || identityProvider == null)
            {
                throw new ArgumentException("UserTokenService identity provider can only be a User");
            }
            var user = identityProvider as User;
            var claims = new List<Claim>
            {
                new Claim(ClaimsIdentity.DefaultNameClaimType, user.Id.ToString()),
                new Claim(ClaimsIdentity.DefaultRoleClaimType, user.IsAdmin? "admin":"user")
            };

            return new ClaimsIdentity(claims,
                                      "Token",
                                      ClaimsIdentity.DefaultNameClaimType,
                                      ClaimsIdentity.DefaultRoleClaimType);
        }
    }
}
