﻿using ASPNetProj.DAL;
using ASPNetProj.DAL.Models;
using ASPNetProj.Services.Interfaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.JsonWebTokens;
using System.IdentityModel.Tokens.Jwt;

namespace ASPNetProj.Services
{
    public class TokenBlocklistManager : ITokenBlocklistManager//Alt background task management: Quartz/hanGfire/hosted service
    {
        private readonly IServiceScopeFactory scopeFactoryService;
        private readonly int Interval;
        //private readonly ILogger Logger;
        private CancellationTokenSource DoCleanup;
        public TokenBlocklistManager(IServiceScopeFactory scopeFactory, IConfiguration configuration)
        {
            scopeFactoryService = scopeFactory;
            if (Int32.TryParse(configuration["Defaults:BlocklistCleanupInterval"], out int result))
            {
                Interval = result;
            }
            else
            {
                Interval = 300000;
            }
            DoCleanup = new CancellationTokenSource();
            DoCleanup.Cancel(); //Communicate the cleanup is NOT running
        }

        ~TokenBlocklistManager()
        {
            DisableCleanup();
        }

        public void DisableCleanup()
        {
            DoCleanup.Cancel();
        }

        public void EnableCleanup()
        {           
            //If not requested -> is currently running
            //Will not start unless the old one is to be cancelled
            if (DoCleanup.IsCancellationRequested)
            {
                DoCleanup = new CancellationTokenSource();
                Cleanup(DoCleanup.Token);
            }
        }

        private async Task Cleanup(CancellationToken cancellation)
        {
            var scope = scopeFactoryService.CreateAsyncScope();
            var context = scope.ServiceProvider.GetRequiredService<DatabaseContext>();
            List<Token> blocklist;
            List<Token> deletionList;
            DateTime now;
            try
            {
                while (!cancellation.IsCancellationRequested)
                {
                    cancellation.ThrowIfCancellationRequested();
                    blocklist = await context.Blocklist.ToListAsync(cancellation);
                    deletionList = new List<Token>(blocklist.Count);
                    now = DateTime.UtcNow;
                    foreach (var token in blocklist)
                    {
                        if (ReadTokenExpirationTime(token.Value) < now)
                        {
                            deletionList.Add(token);
                        }
                    }
                    context.Blocklist.RemoveRange(deletionList);
                    await context.SaveChangesAsync(cancellation);
                    await Task.Delay(Interval, cancellation);
                }
            }
            catch (OperationCanceledException ex)
            {
                //Log
                //Console.WriteLine("Caught OpearationCancelledEx, exiting cleanup");
                return;
            }
        }

        private DateTime ReadTokenExpirationTime(string token)
        {
            var securityToken = new JwtSecurityTokenHandler().ReadJwtToken(token);
            return securityToken.ValidTo;
        }


    }

}
