﻿using ASPNetProj.DAL;
using ASPNetProj.DAL.DTO;
using ASPNetProj.DAL.Models;
using ASPNetProj.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing.Template;
using Microsoft.EntityFrameworkCore;
using System.Net.Mime;

namespace ASPNetProj.Services
{
    public class PageManager : IPageManager
    {
        public int PageSize { get => PageInfo.PageSize; set => PageInfo.PageSize = value; }
        public int PageCount { get => PageInfo.PageCount; set => PageInfo.PageCount = value; }
        private readonly DatabaseContext Context;
        private static PageInfoModel PageInfo;
        private static bool IsUpToDate = false;
        private static List<MoviesPagedListModel> Pages;
        public PageManager(IConfiguration config, DatabaseContext context)
        {
            Context = context;
            if (IsUpToDate)
            {
                return;
            }
            PageInfo = new PageInfoModel();
            if (Int32.TryParse(config["Defaults:MoviePageSize"], out var result))
            {
                PageSize = result;
            }
            PageSize = Math.Max(PageSize, 1);

            var count = context.Movies.Count();
            PageCount = GetPageCount(count);

            Pages = new List<MoviesPagedListModel>(PageCount);
            IsUpToDate = false;
            //RebuildPageList();
        }

        public async Task<MoviesPagedListModel> RequestMoviePage(int index)
        {
            if (!IsUpToDate)
            {
                await RebuildPageList();
            }
            index = Math.Clamp(index, 1, Pages.Count);
            return Pages[index - 1];
        }

        public void SetDirty()
        {
            IsUpToDate = false;
        }

        private async Task RebuildPageList()// LINQ alt: Take()/Skip() queries
        {
            //var list = await Context.Movies.ToListAsync();
            var list = await Context.Movies.Include(movie => movie.Genres).ToListAsync();

            PageCount = GetPageCount(list.Count);

            Pages = new List<MoviesPagedListModel>(PageCount);

            for (int page = 0; page < PageCount; ++page)
            {
                Pages.Add(new MoviesPagedListModel());
                Pages[page].Movies = new List<MovieElementModel>(PageSize);
                Pages[page].PageInfo = new PageInfoModel()
                {
                    PageSize = PageSize,
                    PageCount = PageCount,
                    CurrentPage = page + 1
                };
                var movies = Pages[page].Movies;

                for (int movie = 0; movie < PageSize; ++movie)
                {
                    var index = page * PageSize + movie;
                    if (index >= list.Count)
                    {
                        break;
                    }
                    var currentMovie = list[index];
                    movies.Add(new MovieElementModel()
                    {
                        Id = currentMovie.Id,
                        Name = currentMovie.Name,
                        Poster = currentMovie.Poster,
                        Country = currentMovie.Country,
                        Year = currentMovie.Year,
                        Genres = GetGenres(currentMovie),
                        Reviews = await GetReviews(currentMovie)
                    });
                }
            }
            IsUpToDate = true;
        }

        private static List<GenreModel> GetGenres(Movie movie)
        {
            //var movie = await Context.Movies.FirstOrDefaultAsync(mov => mov.Id == movieId);
            var result = new List<GenreModel>(movie.Genres.Count);
            foreach (var genre in movie.Genres)
            {
                result.Add(new GenreModel()
                {
                    Id = genre.Id,
                    Name = genre.Name
                });
            }
            return result;
        }

        private async Task<List<ReviewShortModel>> GetReviews(Movie movie)
        {
            var result = new List<ReviewShortModel>(movie.Reviews.Count);
            foreach (var review in await Context.Reviews.Where(x=>x.MovieId == movie.Id).ToListAsync())
            {
                result.Add(new ReviewShortModel()
                {
                    Id = review.Id,
                    Rating = review.Rating,
                });
                if (review.IsAnonymous)
                {
                    continue;
                }
            }
            return result;
        }

        private int GetPageCount(int movieCount)
        {
            return Convert.ToInt32(
                Math.Ceiling(
                    Convert.ToDecimal(movieCount) / Convert.ToDecimal(PageSize)));
        }
    }
}
