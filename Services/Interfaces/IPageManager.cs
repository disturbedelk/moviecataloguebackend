﻿using ASPNetProj.DAL.DTO;

namespace ASPNetProj.Services.Interfaces
{
    public interface IPageManager
    {
        int PageCount { get; set; }
        int PageSize { get; set; }

        Task<MoviesPagedListModel> RequestMoviePage(int index);
        void SetDirty();
    }
}