﻿namespace ASPNetProj.Services.Interfaces
{
    public interface ITokenBlocklistManager
    {
        void DisableCleanup();
        void EnableCleanup();
    }
}