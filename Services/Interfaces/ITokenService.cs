﻿using System.Security.Claims;

namespace ASPNetProj.Services.Interfaces
{
    public interface ITokenService
    {
        string BuildToken(ClaimsIdentity identity);
        ClaimsIdentity GetIdentity<T>(T identityProvider);
    }
}