﻿using ASPNetProj.DAL.Models;

namespace ASPNetProj.Services.Interfaces
{
    public interface IMovieModelHelper
    {
        Task FillModelFromMovie(Movie movie, MovieDetailsModel model, Guid? requestingUserId = null);
        void FillModelFromMovie(Movie movie, MovieElementModel model);
        Task FillMovieFromModel(Movie movie, MovieDetailsModel model);
    }
}