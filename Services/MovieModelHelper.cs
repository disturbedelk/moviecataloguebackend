﻿using ASPNetProj.DAL;
using ASPNetProj.DAL.DTO;
using ASPNetProj.DAL.Models;
using ASPNetProj.Services.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Diagnostics;
using System.Diagnostics.Metrics;
using System.IO;
using System.Xml.Linq;

namespace ASPNetProj.Services
{
    public class MovieModelHelper : IMovieModelHelper
    {
        private readonly DatabaseContext Context;
        public MovieModelHelper(DatabaseContext context)
        {
            Context = context;
        }

        public void FillModelFromMovie(Movie movie, MovieElementModel model)
        {
            model.Country = movie.Country;
            model.Name = movie.Name;
            model.Id = movie.Id;
            model.Poster = movie.Poster;
            model.Year = movie.Year;
            model.Reviews = new List<ReviewShortModel>(movie.Reviews.Count);
            model.Genres = new List<GenreModel>(movie.Genres.Count);
            //var reviews = model.Reviews as List<ReviewModel>;

            foreach (var genre in movie.Genres)
            {
                model.Genres.Add(new GenreModel()
                {
                    Id = genre.Id,
                    Name = genre.Name
                });
            }

            foreach (var review in movie.Reviews)
            {
                var currentReview = (new ReviewShortModel()
                {
                    Id = review.Id,
                    Rating = review.Rating,
                });
                model.Reviews.Add(currentReview);
            }

        }

        public async Task FillModelFromMovie(Movie movie, MovieDetailsModel model, Guid? requestingUserId = null)
        {

            model.Id = movie.Id;
            model.Name = movie.Name;
            model.Poster = movie.Poster;
            model.Year = movie.Year;
            model.Country = movie.Country;
            model.Time = movie.Time;
            model.Tagline = movie.Tagline;
            model.Description = movie.Description;
            model.Director = movie.Director;
            model.Budget = movie.Budget;
            model.Fees = movie.Fees;
            model.AgeLimit = movie.AgeLimit;


            if (movie.Genres != null)
            {
                model.Genres = new List<GenreModel>(movie.Genres.Count);
                foreach (var genre in movie.Genres)
                {
                    model.Genres.Add(new GenreModel()
                    {
                        Id = genre.Id,
                        Name = genre.Name
                    });
                }
            }
            if (movie.Reviews == null)
            {
                return;
            }
            model.Reviews = new List<ReviewModel>(movie.Reviews.Count);
            foreach (var review in movie.Reviews)
            {
                var reviewModel = new ReviewModel()
                {
                    Id = review.Id,
                    Rating = review.Rating,
                    CreateDateTime = review.CreateDateTime,
                    IsAnonymous = review.IsAnonymous,
                    ReviewText = review.ReviewText
                };
                reviewModel.Author = new UserShortModel();
                //if anonymous and not yours -> do not fill in the author
                if (review.IsAnonymous && (requestingUserId == null || review.UserId != requestingUserId))
                {
                    model.Reviews.Add(reviewModel);
                    continue;
                }
                var author = await Context.Users.FirstOrDefaultAsync(user => user.Id == review.UserId);
                if (author == null)
                {
                    model.Reviews.Add(reviewModel);
                    continue;
                }
                reviewModel.Author.Avatar = author.AvatarLink;
                reviewModel.Author.NickName = author.Username;
                reviewModel.Author.UserId = review.UserId;
                
                model.Reviews.Add(reviewModel);
            }
        }

        public async Task FillMovieFromModel(Movie movie, MovieDetailsModel model)
        {
            movie.Name = model.Name;
            movie.Poster = model.Poster;
            movie.AgeLimit = model.AgeLimit;
            movie.Budget = model.Budget;
            movie.Country = model.Country;
            movie.Description = model.Description;
            movie.Director = model.Director;
            movie.Fees = model.Fees;
            movie.Tagline = model.Tagline;
            movie.Time = model.Time;
            movie.Year = model.Year;
            movie.Genres.Clear();
            foreach (var genre in model.Genres)
            {
                var found = await Context.Genres.FirstOrDefaultAsync(x => x.Id == genre.Id || x.Name == genre.Name.ToLower());
                if (found != null)
                {
                    if (!movie.Genres.Contains(found))
                    {
                        movie.Genres.Add(found);
                    }
                    continue;
                }
                //genre not found
                var created = new Genre()
                {
                    Id = new Guid(),
                    Name = genre.Name.ToLower()
                };
                movie.Genres.Add(created);
                Context.Genres.Add(created);
            }
            await Context.SaveChangesAsync();
        }
    }
}
